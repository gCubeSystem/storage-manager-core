# Changelog for storage-manager-core

## [v4.1.0]
  * compatible with s3 storage backend
  * remove retry mechanism to exist method
  * add try/catch on exist method #24215
  * update to gcube-bom 4

## [v3.0.0] 2021-09-10
  * fix #22164
  * fix  #21980
  * update gcube-bom version
  * add close operation on IClient interface
  * add check on transport layer instance: if the memory type is not the same, a new transportLayer is instatiated
  * move memoryType var from super class TransportManager
  * convert BasicDBObject to DBObject the return type used for metadata collections
  * One pool for every operation: static Operation class; no mongo close operation
  * upgrade mongo-java-driver to 3.12.0
  * added input parameter to getSize method in order to be compatible with the needed of s3 client
  * moved from version 2.13.1 to 3.0.0-SNAPSHOT

## [v2.9.0] 2019-10-19
  * SSL enabled
  
