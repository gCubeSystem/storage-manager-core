package org.gcube.contentmanagement.blobstorage.service.operation;

import java.io.IOException;
import java.net.URL;
import org.apache.commons.codec.binary.Base64;
import org.gcube.contentmanagement.blobstorage.resource.MemoryType;
import org.gcube.contentmanagement.blobstorage.resource.RequestObject;
import org.gcube.contentmanagement.blobstorage.service.directoryOperation.Encrypter;
import org.gcube.contentmanagement.blobstorage.service.directoryOperation.Encrypter.EncryptionException;
import org.gcube.contentmanagement.blobstorage.transport.TransportManager;
import org.gcube.contentmanagement.blobstorage.transport.TransportManagerFactory;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.contentmanagement.blobstorage.transport.backend.util.Costants;

/**
 * this class is replaced by getHttpsUrl
 * @author roberto
 *
 */
@Deprecated
public class GetHttpUrl extends Operation {

//	private OutputStream os;
	TransportManager tm;
	
	@Deprecated
	public static final String URL_SEPARATOR=Costants.URL_SEPARATOR;
	@Deprecated
	public static final String VOLATILE_URL_IDENTIFICATOR = Costants.VOLATILE_URL_IDENTIFICATOR;
	
	public GetHttpUrl(String[] server, String user, String pwd, String bucket, Monitor monitor, boolean isChunk, String backendType, String[] dbs) {
		super(server, user, pwd, bucket, monitor, isChunk, backendType, dbs);
	}
	
	@Override
	public String initOperation(RequestObject file, String remotePath, String author,
                                String[] server, String rootArea, boolean replaceOption) {
		return getRemoteIdentifier(remotePath, rootArea);
	}

	@Override
	public String initOperation(RequestObject resource, String RemotePath,
                                String author, String[] server, String rootArea) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object doIt(RequestObject requestObject) throws RemoteBackendException {
		String resolverHost= requestObject.getResolverHOst();
		String urlBase="smp://"+resolverHost+Costants.URL_SEPARATOR;
		String urlParam="";
		try {
//			String id=getId(myFile.getAbsoluteRemotePath(), myFile.isForceCreation(), myFile.getGcubeMemoryType(), myFile.getWriteConcern(), myFile.getReadPreference());
			String id=getId(requestObject);
			String phrase= requestObject.getPassPhrase();
//			urlParam =new StringEncrypter("DES", phrase).encrypt(id);
			urlParam = new Encrypter("DES", phrase).encrypt(id);
//			String urlEncoded=URLEncoder.encode(urlParam, "UTF-8");
		} catch (EncryptionException e) {
			throw new RemoteBackendException(" Error in getUrl operation problem to encrypt the string", e.getCause());
		}
		logger.info("URL generated: "+urlBase+urlParam);
		String smpUrl=urlBase+urlParam;
		logger.info("URL generated: "+smpUrl);
		smpUrl=smpUrl.replace("smp://", "http://");
		URL httpUrl=null;
		try {
			httpUrl=translate(new URL(smpUrl));
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info("URL translated: "+httpUrl);
		if(requestObject.getGcubeMemoryType().equals(MemoryType.VOLATILE)){
			return httpUrl.toString()+Costants.VOLATILE_URL_IDENTIFICATOR;
		}
		return httpUrl.toString();
	}

	@Deprecated
	private String getId(String path, boolean forceCreation, MemoryType memoryType, String writeConcern, String readPreference){
		String id=null;
		TransportManagerFactory tmf= new TransportManagerFactory(server, user, password);
		tm=tmf.getTransport(tm, backendType, memoryType, dbNames, writeConcern, readPreference);
		try {
			id = tm.getId(bucket, forceCreation);
		} catch (Exception e) {
			tm.close();
			throw new RemoteBackendException(" Error in GetUrl operation. Problem to discover remote file:"+bucket+" "+ e.getMessage(), e.getCause());			}
		if (logger.isDebugEnabled()) {
			logger.debug(" PATH " + bucket);
		}
		return id;
	}
	
	private String getId(RequestObject requestObject){
		String id=null;
		TransportManager tm=getTransport(requestObject);
		try {
			id = tm.getId(bucket, requestObject.isForceCreation());
		} catch (Exception e) {
			tm.close();
			throw new RemoteBackendException(" Error in GetUrl operation. Problem to discover remote file:"+bucket+" "+ e.getMessage(), e.getCause());			}
		if (logger.isDebugEnabled()) {
			logger.debug(" PATH " + bucket);
		}
		return id;
	}

	
	private URL translate(URL url) throws IOException {
		logger.debug("translating: "+url);
		String urlString=url.toString();
		String baseUrl="http://"+url.getHost()+"/";
		logger.debug("base Url extracted is: "+baseUrl);
//		int index=urlString.lastIndexOf(".org/");
		String params = urlString.substring(baseUrl.length());
		logger.debug("get params: "+baseUrl+" "+params);
		//encode params
		params=Base64.encodeBase64URLSafeString(params.getBytes("UTF-8"));
//		URLEncoder.encode(params, "UTF-8");
		// merge string
		urlString=baseUrl+params;
		logger.info("uri translated in http url: "+urlString);
		return new URL(urlString);
	}
}
