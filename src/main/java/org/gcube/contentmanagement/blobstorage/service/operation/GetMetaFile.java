package org.gcube.contentmanagement.blobstorage.service.operation;

import org.bson.types.ObjectId;
import org.gcube.contentmanagement.blobstorage.resource.RequestObject;
import org.gcube.contentmanagement.blobstorage.service.directoryOperation.BucketCoding;
import org.gcube.contentmanagement.blobstorage.transport.TransportManager;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetMetaFile extends Operation{

	/**
	 * Logger for this class
	 */
    final Logger logger=LoggerFactory.getLogger(GetSize.class);

	public GetMetaFile(String[] server, String user, String pwd,  String bucket, Monitor monitor, boolean isChunk, String backendType, String[] dbs) {
		super(server, user, pwd, bucket, monitor, isChunk, backendType, dbs);
	}
	
	/**
	 * Only the following values will be returned:
	 * mimeType, 
	 * owner, 
	 * id, 
	 * name, 
	 * remotePath, 
	 * size.
	 * 
	 */
	public RequestObject doIt(RequestObject requestObject) throws RemoteBackendException{
		TransportManager tm=getTransport(requestObject);
		long dim=0;
		String id=null;
		String mime=null;
		try {
			dim = tm.getSize(bucket, requestObject);
			id=tm.getId(bucket, false);
			mime=tm.getFileProperty(bucket, "mimetype");
			requestObject.setOwner(tm.getFileProperty(bucket, "owner"));
			if(tm.isValidId(bucket)){
				requestObject.setRemotePath(tm.getFileProperty(bucket, "filename"));
				requestObject.setAbsoluteRemotePath(tm.getFileProperty(bucket, "filename"));
				requestObject.setName(tm.getFileProperty(bucket, "name"));
			}
		} catch (Exception e) {
			tm.close();
			throw new RemoteBackendException(" Error in GetMetaFile operation ", e.getCause());			}
		if (logger.isDebugEnabled()) {
			logger.debug(" PATH " + bucket);
		}
		requestObject.setSize(dim);
		requestObject.setId(id);
		requestObject.setMimeType(mime);

		return requestObject;
	}

	@Override
	public String initOperation(RequestObject file, String remotePath,
                                String author, String[] server, String rootArea, boolean replaceOption) {
//		String[] dirs= remotePath.split(file_separator);
		if(logger.isDebugEnabled())
			logger.debug("remotePath: "+remotePath);
		String buck=null;
		boolean isId=ObjectId.isValid(remotePath);
		if(!isId){
			buck = new BucketCoding().bucketFileCoding(remotePath, rootArea);
			return bucket=buck;
		}else{
			return bucket=remotePath;
		}
	}


	@Override
	public String initOperation(RequestObject resource, String RemotePath,
                                String author, String[] server, String rootArea) {
		throw new IllegalArgumentException("Input/Output stream is not compatible with getSize operation");
	}

}

