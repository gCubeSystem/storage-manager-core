package org.gcube.contentmanagement.blobstorage.service.operation;

import org.gcube.contentmanagement.blobstorage.resource.MemoryType;
import org.gcube.contentmanagement.blobstorage.resource.RequestObject;
import org.gcube.contentmanagement.blobstorage.service.directoryOperation.Encrypter;
import org.gcube.contentmanagement.blobstorage.service.directoryOperation.Encrypter.EncryptionException;
import org.gcube.contentmanagement.blobstorage.transport.TransportManager;
import org.gcube.contentmanagement.blobstorage.transport.TransportManagerFactory;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.contentmanagement.blobstorage.transport.backend.util.Costants;

/**
 * this class is replaced by getHttpsUrl
 * @author roberto
 *
 */
@Deprecated 
public class GetUrl  extends Operation{

//	private OutputStream os;
	TransportManager tm;
	
	public GetUrl(String[] server, String user, String pwd, String bucket, Monitor monitor, boolean isChunk, String backendType, String[] dbs) {
		super(server, user, pwd, bucket, monitor, isChunk, backendType, dbs);
	}
	
	@Override
	public String initOperation(RequestObject file, String remotePath, String author,
                                String[] server, String rootArea, boolean replaceOption) {
		return getRemoteIdentifier(remotePath, rootArea);
	}

	@Override
	public String initOperation(RequestObject resource, String RemotePath,
                                String author, String[] server, String rootArea) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Object doIt(RequestObject requestObject) throws RemoteBackendException {
		String resolverHost= requestObject.getResolverHOst();
		String urlBase="smp://"+resolverHost+Costants.URL_SEPARATOR;
		String urlParam="";
		try {
//			String id=getId(myFile.getAbsoluteRemotePath(), myFile.isForceCreation(), myFile.getGcubeMemoryType(), myFile.getWriteConcern(), myFile.getReadPreference());
			String id=getId(requestObject);
			String phrase= requestObject.getPassPhrase();
			urlParam = new Encrypter("DES", phrase).encrypt(id);
		} catch (EncryptionException e) {
			throw new RemoteBackendException(" Error in getUrl operation problem to encrypt the string", e.getCause());
		}
		String url=urlBase+urlParam;
		logger.info("URL generated: "+url);
		if(requestObject.getGcubeMemoryType().equals(MemoryType.VOLATILE)){
			return url.toString()+Costants.VOLATILE_URL_IDENTIFICATOR;
		}
		return url;
	}
	
	@Deprecated
	private String getId(String path, boolean forceCreation, MemoryType memoryType, String writeConcern, String readPreference){
		String id=null;
		TransportManagerFactory tmf= new TransportManagerFactory(server, user, password);
		tm=tmf.getTransport(tm, backendType, memoryType, dbNames, writeConcern, readPreference);
		try {
			id = tm.getId(bucket, forceCreation);
		} catch (Exception e) {
			tm.close();
			throw new RemoteBackendException(" Error in GetUrl operation. Problem to discover remote file:"+bucket+" "+ e.getMessage(), e.getCause());			}
		if (logger.isDebugEnabled()) {
			logger.debug(" PATH " + bucket);
		}
		return id;
	}
	
	private String getId(RequestObject requestObject){
		String id=null;
		TransportManager tm=getTransport(requestObject);
		try {
			id = tm.getId(bucket, requestObject.isForceCreation());
		} catch (Exception e) {
			tm.close();
			throw new RemoteBackendException(" Error in GetUrl operation. Problem to discover remote file:"+bucket+" "+ e.getMessage(), e.getCause());			}
		if (logger.isDebugEnabled()) {
			logger.debug(" PATH " + bucket);
		}
		return id;
	}
	
}
