package org.gcube.contentmanagement.blobstorage.service.operation;

import org.gcube.contentmanagement.blobstorage.resource.RequestObject;
import org.gcube.contentmanagement.blobstorage.service.directoryOperation.BucketCoding;
import org.gcube.contentmanagement.blobstorage.service.directoryOperation.DirectoryBucket;
import org.gcube.contentmanagement.blobstorage.transport.TransportManager;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.contentmanagement.blobstorage.transport.backend.util.Costants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetUserTotalVolume extends Operation {
	
	final Logger logger=LoggerFactory.getLogger(GetUserTotalVolume.class);

	public GetUserTotalVolume(String[] server, String user, String pwd,  String bucket, Monitor monitor, boolean isChunk, String backendType, String[] dbs) {
		super(server, user, pwd, bucket, monitor, isChunk, backendType, dbs);
	}
	
	public String doIt(RequestObject requestObject) throws RemoteBackendException{
		TransportManager tm=getTransport(requestObject);
		String dim=null;
		logger.info("check user total volume for user: "+getOwner()+ " user is "+user);
		try {
			dim = tm.getUserTotalVolume(getOwner());
		} catch (Exception e) {
			e.printStackTrace();
			tm.close();
			throw new RemoteBackendException(" Error in getUserTotalVolume operation ", e.getCause());			}
		if (logger.isDebugEnabled()) {
			logger.debug(" PATH " + bucket);
		}
		return dim;
	}

	@Override
	public String initOperation(RequestObject file, String remotePath,
                                String author, String[] server, String rootArea, boolean replaceOption) {
		setOwner(author);
		if(remotePath!= null && remotePath.length()>0){
//			String[] dirs= remotePath.split(file_separator);
			if(logger.isDebugEnabled())
				logger.debug("remotePath: "+remotePath);
			String buck=null;
			BucketCoding bc=new BucketCoding();
			buck=bc.bucketFileCoding(remotePath, rootArea);
			if(!Costants.CLIENT_TYPE.equalsIgnoreCase("mongo")){
				buck=buck.replaceAll(Costants.FILE_SEPARATOR, Costants.SEPARATOR);
			//remove directory bucket		
				DirectoryBucket dirBuc=new DirectoryBucket(server,user, password, remotePath, author, getRegion(), getToken());
				dirBuc.removeKeysOnDirBucket(file, buck, rootArea, backendType, dbNames);
			}
			return bucket=buck;
		}else{
			return bucket;
		}
		
	}


	@Override
	public String initOperation(RequestObject resource, String RemotePath,
                                String author, String[] server, String rootArea) {
		throw new IllegalArgumentException("Input/Output stream is not compatible with getSize operation");
	}


}
