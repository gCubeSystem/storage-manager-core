package org.gcube.contentmanagement.blobstorage.transport;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;

import org.gcube.contentmanagement.blobstorage.resource.MemoryType;
import org.gcube.contentmanagement.blobstorage.transport.backend.MongoOperationManager;
import org.gcube.contentmanagement.blobstorage.transport.backend.util.Costants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import terrastore.client.TerrastoreClient;
/**
 * Transport manager factory
 * @author Roberto Cirillo (ISTI - CNR)
 *
 */
public class TransportManagerFactory {
	
	/**
	 * Logger for this class
	 */
//	private static final Logger logger = Logger.getLogger(OperationFactory.class);
	final Logger logger = LoggerFactory.getLogger(TransportManagerFactory.class);
//	TerrastoreClient client;
	private String[] server;
	private String user;
	private String password;
	private MemoryType memoryType;
	private String dbNames;
	private String token;
	private String region;
	TransportManager transport;
	
	public TransportManagerFactory(String[] server, String user, String password){
		this.server=server;
		this.user=user;
		this.password=password;
		this.region=region;
		this.token=token;
	}
	
	public TransportManager getTransport(TransportManager tm, String backendType, MemoryType memoryType, String[] dbNames, String writeConcern, String readConcern){
		if (logger.isDebugEnabled()) {
			logger.debug("getOperation(String) - start");
		}
		if(logger.isDebugEnabled() && (!Objects.isNull(transport)))
			logger.debug("transportLayer with "+transport.memoryType+" already instatiated. New memoryType request is "+memoryType);
// if we haven't any transport layer instantiated or the transport layer is istantiated on another memory type (persistent, volatile), 
// then a new transport layer is needed		
		if(Objects.isNull(tm) || Objects.isNull(tm.memoryType) || (!tm.memoryType.equals(memoryType))) {
			logger.info("new transport layer instantiated for "+memoryType+" memory");
			return load(backendType, memoryType, dbNames, writeConcern, readConcern);
		}else {
			logger.debug("new transport layer not instantiated.");
		}
		return tm;
	}
	
	private TransportManager  load(String backendType, MemoryType memoryType, String[] dbNames, String writeConcern, String readConcern){
		ServiceLoader<TransportManager> loader = ServiceLoader.load(TransportManager.class);
		Iterator<TransportManager> iterator = loader.iterator();
		List<TransportManager> impls = new ArrayList<TransportManager>();
		logger.info("Try to load the backend...");
		logger.info("the specified backend passed as input param is "+backendType);
		 while(iterator.hasNext())
			 impls.add(iterator.next());
		 int implementationCounted=impls.size();
//		 System.out.println("size: "+implementationCounted);
		 if((implementationCounted==0) || backendType.equals(Costants.DEFAULT_TRANSPORT_MANAGER)){
			 logger.info(" 0 implementation found. Load default implementation of TransportManager");
			 return new MongoOperationManager(server, user, password, memoryType, dbNames, writeConcern, readConcern);
		 }else if((implementationCounted==1) && (!Objects.isNull(backendType))){
			 TransportManager tm = impls.get(0);
			 logger.info("1 implementation of TransportManager found. Load it. "+tm.getName());
			 tm.initBackend(server, user, password, memoryType, dbNames, writeConcern, readConcern, token, region);
			 return tm;
		 }else{
			 logger.info("found "+implementationCounted+" implementations of TransportManager");
			 logger.info("search: "+backendType);
			 for(TransportManager tm : impls){
				 if(tm.getName().equalsIgnoreCase(backendType)){
					 logger.info("Found implementation "+backendType);
					 tm.initBackend(server, user, password, memoryType, dbNames, writeConcern, readConcern, token,region);
					 return tm;
				 }
			 }
			 throw new IllegalStateException("Mismatch Backend Type and RuntimeResource Type. The backend type expected is "+backendType);
		 }
		 
	}


}
